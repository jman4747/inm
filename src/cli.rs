/*
 * MIT License
 * Copyright (c) 2022. Joshua Turner
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

use crate::*;
use clap::{Parser, Subcommand};
use rust_decimal::prelude::*;
use std::io::{BufWriter, Result, Write};

#[derive(Debug, Parser)]
#[clap(author, version, about, long_about = None)]
#[clap(propagate_version = true)]
pub struct Cli {
    /// Number of decimal places to display on output
    #[clap(default_value_t = 3)]
    decimals: i8,

    #[clap(subcommand)]
    command: Commands,
}

#[derive(Debug, Subcommand)]
pub enum Commands {
    /// Convert from inches to mm
    In {
        /// Inch value to convert the mm
        value: f64,
    },
    /// Convert from mm to inches
    Mm {
        /// MM value to convert to Inch
        value: f64,
    },
}

pub fn run_cli() -> Result<()> {
    let cli = Cli::parse();

    let input = match cli.command {
        Commands::In { value } => Input {
            value: match Decimal::from_f64(value) {
                Some(v) => v,
                _ => Decimal::ZERO,
            },
            decimals: cli.decimals,
            unit: Unit::Inch,
        },

        Commands::Mm { value } => Input {
            value: match Decimal::from_f64(value) {
                Some(v) => v,
                _ => Decimal::ZERO,
            },
            decimals: cli.decimals,
            unit: Unit::Mm,
        },
    };

    print_ans(Output::get(&input))?;
    Ok(())
}

fn print_ans(output: Output) -> Result<()> {
    // write ans to console: https://nnethercote.github.io/perf-book/io.html
    let stdout = std::io::stdout();
    let lock = stdout.lock();
    let mut buf = BufWriter::new(lock);

    //input
    write!(buf, "\n")?;
    writeln!(buf, "inm \"Inch to MM\" version: {}\n", VERSION)?;
    writeln!(buf, "[input]")?;
    writeln!(buf, "value = {}", output.inputs_used.value)?;
    writeln!(buf, "unit = {}", output.inputs_used.unit.name())?;
    write!(buf, "decimal_places = {}\n\n", output.inputs_used.decimals)?;

    //answer
    writeln!(buf, "[answer]")?;
    writeln!(buf, "value = {}", output.output_value)?;
    writeln!(buf, "unit = {}\n", output.output_unit.name())?;
    buf.flush()?;
    Ok(())
}
