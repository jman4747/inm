/*
 * MIT License
 * Copyright (c) 2022. Joshua Turner
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

pub mod cli;
use rust_decimal::prelude::*;
use rust_decimal_macros::dec;

const CONVERSION_FACTOR: Decimal = dec!(25.4);

const VERSION: &str = env!("CARGO_PKG_VERSION");

fn in_to_mm(inch: Decimal) -> Decimal {
    inch * CONVERSION_FACTOR
}

fn mm_to_in(mm: Decimal) -> Decimal {
    mm / CONVERSION_FACTOR
}

#[derive(PartialEq, Debug)]
pub enum Unit {
    Inch,
    Mm,
}

impl Unit {
    fn name(&self) -> &str {
        match *self {
            Unit::Inch => "Inches",
            Unit::Mm => "Millimeters",
        }
    }
}

pub struct Input {
    value: Decimal,
    decimals: i8,
    unit: Unit,
}

pub struct Output<'a> {
    inputs_used: &'a Input,
    output_value: Decimal,
    output_unit: Unit,
}

impl<'a> Output<'a> {
    fn get(i: &Input) -> Output {
        match i.unit {
            Unit::Inch => Output {
                inputs_used: i,
                output_value: round(in_to_mm(i.value), i.decimals),
                output_unit: Unit::Mm,
            },
            Unit::Mm => Output {
                inputs_used: i,
                output_value: round(mm_to_in(i.value), i.decimals),
                output_unit: Unit::Inch,
            },
        }
    }
}

fn round(x: Decimal, f: i8) -> Decimal {
    x.round_dp_with_strategy(f as u32, RoundingStrategy::MidpointAwayFromZero)
}

#[test]
fn test_in_to_mm() {
    assert_eq!(in_to_mm(dec!(1.4375)), dec!(36.5125));
    assert_eq!(in_to_mm(dec!(2.0)), dec!(50.8))
}

#[test]
fn test_mm_to_in() {
    assert_eq!(mm_to_in(dec!(12.7)), dec!(0.5))
}

#[test]
fn test_output_get() {
    let input = Input {
        value: dec!(1.4375),
        unit: Unit::Inch,
        decimals: 3,
    };

    let output = Output::get(&input);

    assert_eq!(output.output_unit, Unit::Mm);
    assert_eq!(output.output_value, dec!(36.513));
}

#[test]
fn test_round() {
    assert_eq!(round(dec!(36.5125), 3), dec!(36.513));
    assert_eq!(round(dec!(34.925), 2), dec!(34.93));
    assert_eq!(round(dec!(2.12345), 4), dec!(2.1235))
}
